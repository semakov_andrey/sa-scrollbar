'use strict';

const packageJSON               = require('../package.json');
const config                    = require('./webpack.common.js');
const webpackMerge              = require('webpack-merge');
const path                      = require('path');
const autoprefixer              = require('autoprefixer');
const PostCSSAssetsPlugin       = require('postcss-assets-webpack-plugin');
const MiniCssExtractPlugin      = require('mini-css-extract-plugin');
const cssnano                   = require('cssnano');
const UglifyJsPlugin            = require('uglifyjs-webpack-plugin');
const dirs                      = packageJSON.config.directories;
const browserList               = packageJSON.config.browsers;
const entries                   = packageJSON.config.entries;

Object.keys(entries).forEach(key => {
  entries[key] = [ path.resolve(dirs.source, entries[key]) ];
  if (key.indexOf('scrollbar') === -1) {
    delete entries[key];
  }
});

module.exports = webpackMerge(config, {
  entry: entries,
  mode: 'production',
  output: {
    filename: '[name].js',
    library: 'saScrollbar',
    libraryTarget: 'umd',
    path: path.resolve(dirs.production)
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css'
    })
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        test: /\.js$/,
        uglifyOptions: {
          output: {
            comments: false
          }
        }
      }),
      new PostCSSAssetsPlugin({
        test: /\.css$/,
        log: false,
        plugins: [
          autoprefixer({
            browsers: browserList
          }),
          cssnano({
            preset: ['default', {
              discardComments: {
                removeAll: true
              },
              minifyFontValues: {
                removeQuotes: false
              }
            }]
          })
        ]
      })
    ]
  },
  performance: {
    hints: 'warning',
    maxEntrypointSize: 512000,
    maxAssetSize: 4096000
  }
});