'use strict';

const dirs                      = require('../package.json').config.directories;

Object.keys(dirs.files).forEach(folder => {
  if (dirs.files[folder] !== '') {
    dirs.files[folder] += '/';
  }
});

module.exports = {
  output: {
    filename: 'scrollbar.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: [ '@babel/preset-env', '@babel/preset-react' ],
            plugins: [ '@babel/plugin-proposal-class-properties' ]
          }
        }]
      }
    ]
  }
};