import ResizeObserver from 'resize-observer-polyfill';
import Track from './track.js';
import './scrollbar.scss';

const instances = {};

class SaScrollbar {
  constructor(selector = ':root') {
    if (this.isTouchDevice()) return;
    if (instances[selector]) return instances[selector];
    instances[selector] = this;
    this.selector = selector;
    this.create();
    this.init();
    this.bindEvents();
  }

  isTouchDevice = () => 'ontouchstart' in document

  create() {
    const element = document.querySelector(this.selector);
    element.classList.add('scrolling');
    if (this.selector !== ':root') {
      element.innerHTML = `<div class="scrolling-wrapper"><div class="scrolling-content">${element.innerHTML}</div></div>`;
      this.element = element.children[0];
      this.content = this.element.children[0];
      this.destination = element;
    } else {
      this.element = element;
      this.destination = document.body;
    }
  }

  init = () => {
    ({ scrollHeight: this.scrollHeight, scrollWidth: this.scrollWidth } = this.element);
    const { clientHeight, clientWidth } = this.element;
    const scrollTopMax = this.scrollHeight - clientHeight;
    const scrollLeftMax = this.scrollWidth - clientWidth;
    const style = window.getComputedStyle(this.element);
    const hiddenX = style['overflow-x'] === 'hidden';
    const hiddenY = style['overflow-y'] === 'hidden';
    let conflict = false;
    if (scrollTopMax && scrollLeftMax) conflict = true;
    if (scrollTopMax) {
      const params = {
        element: this.element,
        destination: this.destination,
        scrollWidth: this.scrollHeight,
        clientWidth: clientHeight,
        scrollMax: scrollTopMax,
        hidden: hiddenY,
        conflict
      };
      if (this.verticalBar) {
        this.verticalBar.init(params);
        this.verticalBar.update();
      } else {
        this.verticalBar = new Track({
          direction: 1,
          scrollTo: this.scrollTo,
          ...params
        });
      }
    } else if (this.verticalBar) {
      this.verticalBar.destructor();
      delete this.verticalBar;
    }
    if (scrollLeftMax) {
      const params = {
        element: this.element,
        destination: this.destination,
        scrollWidth: this.scrollWidth,
        clientWidth,
        scrollMax: scrollLeftMax,
        hidden: hiddenX,
        conflict
      };
      if (this.horizontalBar) {
        this.horizontalBar.init(params);
        this.horizontalBar.update();
      } else {
        this.horizontalBar = new Track({
          direction: 0,
          scrollTo: this.scrollTo,
          ...params
        });
      }
    } else if (this.horizontalBar) {
      this.horizontalBar.destructor();
      delete this.horizontalBar;
    }
  }

  updateBar = () => {
    if (this.verticalBar) this.verticalBar.update();
    if (this.horizontalBar) this.horizontalBar.update();
  }

  bindEvents() {
    this.observer = new ResizeObserver(() => this.init());
    if (this.selector === ':root') {
      document.addEventListener('scroll', this.updateBar);
      window.addEventListener('resize', this.init);
      this.observer.observe(this.destination);
    } else {
      this.element.addEventListener('scroll', this.updateBar);
      this.observer.observe(this.content);
    }
  }

  destructor() {
    const element = document.querySelector(this.selector);
    element.classList.remove('scrolling');
    if (this.verticalBar) {
      this.verticalBar.destructor();
      delete this.verticalBar;
    }
    if (this.horizontalBar) {
      this.horizontalBar.destructor();
      delete this.horizontalBar;
    }
    if (this.selector === ':root') {
      document.removeEventListener('scroll', this.updateBar);
      window.removeEventListener('resize', this.init);
      this.observer.unobserve(this.destination);
    } else {
      this.element.removeEventListener('scroll', this.updateBar);
      this.observer.unobserve(this.content);
      element.innerHTML = this.content.innerHTML;
    }
    delete this.observer;
  }
}

export default SaScrollbar;
