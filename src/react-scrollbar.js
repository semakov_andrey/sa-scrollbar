import React from 'react';
import Track from './react-track';
import ResizeObserver from 'resize-observer-polyfill';
import './react-scrollbar.scss';

const DEFAULT_CONFLICT_SIZE = 14;
const MIN_THUMB_SIZE = 20;

export default class Scrollbar extends React.PureComponent {
  scrolling = React.createRef()

  element = React.createRef()

  content = React.createRef()

  scrollbar = {
    horizontal: React.createRef(),
    vertical: React.createRef()
  }

  isTouchDevice = () => 'ontouchstart' in document

  isSafari = () => /Version\/[\d\.]+.*Safari/.test(navigator.userAgent)

  isEdge = () => /Edge\/\d./i.test(navigator.userAgent)

  scrollInBody = this.isEdge() || this.isSafari()

  state = {
    root: this.props.selector === 'root',
    custom: typeof this.props.selector === 'object' && this.props.selector,
    horizontal: {
      type: 'horizontal',
      prop: 'width',
      axis: 'X',
      padding: 'left'
    },
    vertical: {
      type: 'vertical',
      prop: 'height',
      axis: 'Y',
      padding: 'top'
    },
    conflict: 0
  }

  componentDidMount() {
    const { root, custom } = this.state;
    if (root) {
      document.documentElement.classList.add('scrolling');
      document.body.classList.add('scrolling-wrapper');
    }
    if (custom) {
      const { current } = this.scrolling;
      const element = current.querySelector(custom.element);
      const content = current.querySelector(custom.content);
      element.classList.add('scrolling-wrapper');
      content.classList.add('scrolling-content');
      this.element = { current: element };
      this.content = { current: content };
    }
    if (!this.isTouchDevice()) {
      this.setSizes();
      this.observer = new ResizeObserver(() => this.setSizes());
      if (this.state.root) {
        document.addEventListener('scroll', this.scrollHandler);
        window.addEventListener('resize', this.setSizes);
      } else {
        this.element.current.addEventListener('scroll', this.scrollHandler);
        this.observer.observe(this.element.current);
      }
      this.observer.observe(this.content.current);
    }
  }

  componentWillUnmount() {
    const { root, custom } = this.state;
    if (root) {
      document.documentElement.classList.remove('scrolling');
      document.body.classList.remove('scrolling-wrapper');
    }
    if (custom) {
      this.element.current.classList.remove('scrolling-wrapper');
      this.content.current.classList.remove('scrolling-content');
    }
    if (!this.isTouchDevice()) {
      if (this.state.root) {
        document.removeEventListener('scroll', this.scrollHandler);
        window.removeEventListener('resize', this.setSizes);
      } else {
        this.element.current.removeEventListener('scroll', this.scrollHandler);
        this.observer.unobserve(this.element.current);
      }
      this.observer.unobserve(this.content.current);
    }
  }

  setSizes = () => {
    const { root, conflict } = this.state;
    const { clientWidth, clientHeight } = root ? document.documentElement : this.element.current;
    const scrollWidth = this.content.current.offsetWidth;
    const scrollHeight = this.content.current.offsetHeight;

    const scrollLeftMax = scrollWidth - clientWidth;
    const scrollTopMax = scrollHeight - clientHeight;

    const barWidth = clientWidth - (root ? conflict : 0);
    const barHeight = clientHeight - (root ? conflict : 0);
    const naturalWidth = Math.ceil(clientWidth * barWidth / scrollWidth);
    const naturalHeight = Math.ceil(clientHeight * barHeight / scrollHeight);
    const sizeWidth = naturalWidth < MIN_THUMB_SIZE ? MIN_THUMB_SIZE : naturalWidth;
    const sizeHeight = naturalHeight < MIN_THUMB_SIZE ? MIN_THUMB_SIZE : naturalHeight;
    const thumbMaxWidth = barWidth - sizeWidth;
    const thumbMaxHeight = barHeight - sizeHeight;
    const scaleFactorWidth = (sizeWidth + 6) / sizeWidth;
    const scaleFactorHeight = (sizeHeight + 6) / sizeHeight;

    this.setState(state => ({
      horizontal: {
        ...state.horizontal,
        clientWidth,
        scrollWidth,
        scrollMax: scrollLeftMax,
        naturalWidth,
        size: sizeWidth,
        thumbMax: thumbMaxWidth,
        scaleFactor: scaleFactorWidth
      },
      vertical: {
        ...state.vertical,
        clientWidth: clientHeight,
        scrollWidth: scrollHeight,
        scrollMax: scrollTopMax,
        naturalWidth: naturalHeight,
        size: sizeHeight,
        thumbMax: thumbMaxHeight,
        scaleFactor: scaleFactorHeight
      },
      conflict: scrollLeftMax && scrollTopMax ? this.getCoflictSize() : 0
    }));
  }

  getCoflictSize = () => {
    const size = parseInt(window.getComputedStyle(document.documentElement).getPropertyValue('--sa-width-conflict'), 10);
    return Number.isNaN(size) ? DEFAULT_CONFLICT_SIZE : size;
  }

  scrollElement = () => this.state.root ? (this.scrollInBody ? document.body : document.documentElement) : this.element.current;

  scrollHandler = () => {
    const { scrollLeft, scrollTop } = this.scrollElement();
    this.changePosition('horizontal', scrollLeft);
    this.changePosition('vertical', scrollTop);
  }

  scrollTo = padding => value => {
    this.scrollElement()[padding] = value;
  }

  changePosition = (direction, scroll) => {
    if (this.scrollbar[direction].current) {
      const { axis, scrollMax, thumbMax } = this.state[direction];
      const position = Math.ceil(scroll * thumbMax / scrollMax);
      this.scrollbar[direction].current.style.transform = `translate${axis}(${position}px) translateZ(0)`;
    }
  }

  classNames = (...args) => args.filter(arg => arg).join(' ')

  renderTracks() {
    const { root, horizontal, vertical, conflict } = this.state;
    return !this.isTouchDevice() ? (
      <>
        { horizontal.scrollMax ? (
          <Track
            { ...horizontal }
            root={ root }
            conflict={ conflict }
            scrollTo={ this.scrollTo('scrollLeft') }
            link={ this.scrollbar.horizontal }
          />
        ) : null }
        { vertical.scrollMax ? (
          <Track
            { ...vertical }
            root={ root }
            conflict={ conflict }
            scrollTo={ this.scrollTo('scrollTop') }
            link={ this.scrollbar.vertical }
          />
        ) : null }
      </>
    ) : null;
  }

  render() {
    const { children, className } = this.props;
    const { root, custom, horizontal, vertical } = this.state;
    if (custom) {
      return (
        <div
          className={
            this.classNames(
              'scrolling',
              className ? className : '',
              horizontal.scrollMax ? 'scrolling_bottom' : '',
              vertical.scrollMax ? 'scrolling_right' : ''
            )
          }
          ref={ this.scrolling }
        >
          { children }
          { this.renderTracks() }
        </div>
      );
    }
    return root
      ? (
        <>
          <div className="scrolling-content" ref={ this.content }>{ children }</div>
          { this.renderTracks() }
        </>
      ) : (
        <div
          className={
            this.classNames(
              'scrolling',
              className ? className : '',
              horizontal.scrollMax ? 'scrolling_bottom' : '',
              vertical.scrollMax ? 'scrolling_right' : ''
            )
          }
        >
          <div className="scrolling-wrapper" ref={ this.element }>
            <div className="scrolling-content" ref={ this.content }>{ children }</div>
          </div>
          { this.renderTracks() }
        </div>
      );
  }
}