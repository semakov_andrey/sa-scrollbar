import './index.html';
import './index.scss';
import React              from 'react';
import ReactDOM           from 'react-dom';
import Children           from './children';
import Scrollbar          from '../react-scrollbar';

ReactDOM.render(
  <Scrollbar selector="root">
    <Children/>
    <Scrollbar className="main-scrolling">
      <Children/>
    </Scrollbar>
  </Scrollbar>,
  document.getElementById('app')
);