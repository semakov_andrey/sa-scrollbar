# SA Scrollbar. #
###### Scrollbar for your project. ######

### Installation ###  
`npm install sa-scrollbar --save`

### Connection ###  
`import Scrollbar from 'sa-scrollbar';`  
`import sa-scrollbar/build/scrollbar.css`  
`new Scrollbar();`  
or  
`new Scrollbar('.selector');`  

### Changing color ###  
`--sa-color-view: #f06000;`  