'use strict';

const packageJSON               = require('../package.json');
const config                    = require('./webpack.common.js');
const webpackMerge              = require('webpack-merge');
const path                      = require('path');
const autoprefixer              = require('autoprefixer');
const MiniCssExtractPlugin      = require('mini-css-extract-plugin');
const dirs                      = packageJSON.config.directories;
const browserList               = packageJSON.config.browsers;
const entries                   = packageJSON.config.entries;
const configServer           	  = packageJSON.config.devServer;

Object.keys(entries).forEach((key, index) => {
  entries[key] = [ path.resolve(dirs.source, entries[key]) ];
  if (!index) {
    entries[key].unshift(`webpack-dev-server/client?http://localhost:${configServer.port}`);
  }
  if (key.indexOf('scrollbar') !== -1) {
    delete entries[key];
  }
});

module.exports = webpackMerge(config, {
  entry: entries,
  mode: 'development',
  output: {
    path: path.resolve(dirs.development)
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: `${dirs.files.html}[name].html`
            }
          },
          'extract-loader',
          {
            loader: 'html-loader',
            options: {
              interpolate: 'require'
            }
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: () => [
                autoprefixer({
                  browsers: browserList
                })
              ]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              outputStyle: 'expanded',
              sourceMap: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'scrollbar.css'
    })
  ],
  devServer: {
    contentBase: path.resolve('../', dirs.development),
    compress: true,
    disableHostCheck: true,
    https: false,
    inline: true,
    lazy: false,
    quiet: false,
    stats: 'minimal',
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  },
  devtool: 'source-map'
});