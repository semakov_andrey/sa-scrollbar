export default class Track {
  names = {
    vertical: {
      name: 'vertical',
      axis: 'Y',
      size: 'Height',
      padding: 'Top'
    },
    horizontal: {
      name: 'horizontal',
      axis: 'X',
      size: 'Width',
      padding: 'Left'
    }
  }

  constructor(params) {
    ({ name: this.nName, axis: this.nAxis, size: this.nSize, padding: this.nPadding } = this.getNames(params.direction));
    this.element = (this.isEdge() || this.isSafari()) && params.element === document.documentElement ? params.scrolling : params.element;
    this.scrolling = params.scrolling;
    this.minThumbSize = 20;
    this.savedPosition = 0;
    this.scrollTo = params.scrollTo;
    this.create();
    this.init(params);
    this.update();
    this.bindEvents();
  }

  destructor() {
    this.unbindEvents();
    this.destroy();
  }

  isSafari = () => /Version\/[\d\.]+.*Safari/.test(navigator.userAgent)

  isEdge = () => /Edge\/\d./i.test(navigator.userAgent)

  getNames = direction => this.names[direction ? 'vertical' : 'horizontal']

  create() {
    this.bar = document.createElement('div');
    this.track = document.createElement('div');
    this.thumb = document.createElement('div');
    this.view = document.createElement('div');
    this.bar.classList.add('scrollbar');
    this.bar.classList.add(`scrollbar_${this.nName}`);
    this.track.classList.add('scrollbar__track');
    this.thumb.classList.add('scrollbar__thumb');
    this.view.classList.add('scrollbar__view');
    this.bar.appendChild(this.track);
    this.bar.appendChild(this.thumb);
    this.thumb.appendChild(this.view);
    this.scrolling.appendChild(this.bar);
  }

  destroy() {
    this.bar.parentNode.removeChild(this.bar);
  }

  init(params) {
    ({ scrollWidth: this.scrollWidth, clientWidth: this.clientWidth, scrollMax: this.scrollMax, hidden: this.hidden, conflict: this.conflict } = params);
    this.bar.classList[this.hidden ? 'add' : 'remove']('scrollbar_hidden');
    this.bar.classList[this.conflict ? 'add' : 'remove']('scrollbar_scaled');
    this.naturalThumbWidth = Math.ceil(this.clientWidth * this.bar[`client${this.nSize}`] / this.scrollWidth);
    this.visibleThumbWidth = 0;
    if (this.naturalThumbWidth < this.minThumbSize) {
      this.visibleThumbWidth = this.minThumbSize;
    } else if (this.scrollMax) {
      this.visibleThumbWidth = this.naturalThumbWidth;
    }
    this.thumbMax = this.bar[`client${this.nSize}`] - this.visibleThumbWidth;
    this.thumb.style[this.nSize.toLowerCase()] = `${this.visibleThumbWidth}px`;
    this.view.style.setProperty('--sa-scale-factor', (this.visibleThumbWidth + 6) / this.visibleThumbWidth);
  }

  update() {
    const position = Math.ceil(this.element[`scroll${this.nPadding}`] * this.thumbMax / this.scrollMax);
    this.thumb.style.transform = `translate${this.nAxis}(${position}px) translateZ(0)`;
  }

  bindEvents() {
    this.track.addEventListener('mousedown', this.mouseDownTrackHandler);
    this.thumb.addEventListener('mousedown', this.mouseDownThumbHandler);
    this.thumb.addEventListener('mouseup', this.mouseUpThumbHandler);
    document.addEventListener('mouseup', this.mouseUpThumbHandler);
  }

  unbindEvents() {
    this.track.removeEventListener('mousedown', this.mouseDownTrackHandler);
    this.thumb.removeEventListener('mousedown', this.mouseDownThumbHandler);
    this.thumb.removeEventListener('mouseup', this.mouseUpThumbHandler);
    document.removeEventListener('mouseup', this.mouseUpThumbHandler);
  }

  mouseDownTrackHandler = event => {
    if (event.target !== event.currentTarget) return;
    this.element[`scroll${this.nPadding}`] = (event[`offset${this.nAxis}`] - this.naturalThumbWidth * .5) * this.scrollWidth / this.clientWidth;
    this.update();
  }

  mouseDownThumbHandler = event => {
    this.bar.classList.add('scrollbar_active');
    this.savedPosition = this.visibleThumbWidth - event[`offset${this.nAxis}`];
    document.addEventListener('mousemove', this.mouseMoveDocumentHandler);
    document.onselectstart = () => false;
  }

  mouseMoveDocumentHandler = event => {
    if (this.savedPosition) {
      const offset = event[`client${this.nAxis}`] - this.bar.getBoundingClientRect()[this.nPadding.toLowerCase()];
      const thumbClickPosition = this.visibleThumbWidth - this.savedPosition;
      this.element[`scroll${this.nPadding}`] = this.scrollMax * (offset - thumbClickPosition) / this.thumbMax;
      this.update();
    }
  }

  mouseUpThumbHandler = () => {
    this.bar.classList.remove('scrollbar_active');
    document.removeEventListener('mousemove', this.mouseMoveDocumentHandler);
    document.onselectstart = null;
  }
}