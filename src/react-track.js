import React from 'react';

export default class ReactTrack extends React.PureComponent {
  state = {
    active: false,
    savedPosition: null
  }

  bar = React.createRef()

  componentDidMount() {
    document.addEventListener('mouseup', this.mouseUpThumbHandler);
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.mouseUpThumbHandler);
  }

  mouseDownTrackHandler = event => {
    if (event.target !== event.currentTarget) return;
    const { scrollTo, axis, clientWidth, scrollWidth, naturalWidth } = this.props;
    scrollTo((event.nativeEvent[`offset${axis}`] - naturalWidth * .5) * scrollWidth / clientWidth);
  }

  mouseDownThumbHandler = event => {
    const { axis, size } = this.props;
    this.setState({ active: true, savedPosition: size - event.nativeEvent[`offset${axis}`] });
    document.addEventListener('mousemove', this.mouseMoveDocumentHandler);
    document.onselectstart = () => false;
  }

  mouseMoveDocumentHandler = event => {    
    const { scrollTo, axis, scrollMax, padding, thumbMax, size } = this.props;
    const { savedPosition } = this.state;
    if (savedPosition) {
      const offset = event[`client${axis}`] - this.bar.current.getBoundingClientRect()[padding];
      const thumbClickPosition = size - savedPosition;
      scrollTo(scrollMax * (offset - thumbClickPosition) / thumbMax);
    }
  }

  mouseUpThumbHandler = () => {
    this.setState({ active: false, savedPosition: null });
    document.removeEventListener('mousemove', this.mouseMoveDocumentHandler);
    document.onselectstart = null;
  }

  classNames = (...args) => args.filter(arg => arg).join(' ')

  render() {
    const { type, prop, root, conflict, link, size, scaleFactor } = this.props;
    const { active } = this.state;
    return (
      <div
        className={
          this.classNames(
            'scrollbar',
            `scrollbar_${type}`,
            root ? 'scrollbar_root' : '',
            conflict ? 'scrollbar_scaled' : '',
            active ? 'scrollbar_active' : ''
          )
        }
        ref={ this.bar }
      >
        <div className="scrollbar__track" onMouseDown={ this.mouseDownTrackHandler }></div>
        <div
          className="scrollbar__thumb"
          onMouseDown={ this.mouseDownThumbHandler }
          onMouseUp={ this.mouseUpThumbHandler }
          style={ { [prop]: `${size}px` } }
          ref={ link }
        >
          <div className="scrollbar__view" style={ { '--sa-scale-factor': scaleFactor } }/>
        </div>
      </div>
    );
  }
}